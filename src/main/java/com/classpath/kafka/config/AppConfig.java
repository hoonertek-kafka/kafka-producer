package com.classpath.kafka.config;

public class AppConfig {

	public static final Object CLIENT_ID = "PRADEEP_PRODUCER_APP";
	public static final Object BOOTSTRAP_SERVERS = "206.189.128.35:9092";
	public static final String TOPIC = "customers-data";
}
